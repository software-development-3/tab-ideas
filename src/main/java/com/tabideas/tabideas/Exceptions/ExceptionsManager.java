package com.tabideas.tabideas.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsManager {
    @ExceptionHandler(UserRegisteredException.class)
    private ResponseEntity<String> userAlreadyExists(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new UserRegisteredException().getMessage());
    }
}
