package com.tabideas.tabideas.Exceptions;

public class UserRegisteredException extends RuntimeException {
    public UserRegisteredException(){
        super("User already exists");
    }

}
