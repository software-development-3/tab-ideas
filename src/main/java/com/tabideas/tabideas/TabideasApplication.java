package com.tabideas.tabideas;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@OpenAPIDefinition(info = @Info(title = "TabIdeas API User", version = "1.0.0", description = "API responsible for applying CRUD to users"))
public class TabideasApplication {

	public static void main(String[] args) {
		SpringApplication.run(TabideasApplication.class, args);
	}

}
