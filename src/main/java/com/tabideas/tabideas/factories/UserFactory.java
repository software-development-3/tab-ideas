package com.tabideas.tabideas.factories;

import com.tabideas.tabideas.dtos.ResponseUserDTO;
import com.tabideas.tabideas.entities.UserEntity;

import java.util.List;
import java.util.stream.Collectors;

public class UserFactory {
    public static List<ResponseUserDTO> convertUsersToResponseDTO(List<UserEntity> users){
        return users.stream()
                .map(user -> new ResponseUserDTO(user.getId(), user.getName(), user.getLogin()))
                .collect(Collectors.toList());
    }

    public static ResponseUserDTO convertUserToResponseDTO(UserEntity user) {
        return new ResponseUserDTO(user.getId(), user.getName(), user.getLogin());
    }
}
