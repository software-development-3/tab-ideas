package com.tabideas.tabideas.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record AuthenticationDTO(
        @Email @NotNull @NotBlank
        String login,
        @NotNull @NotBlank
        String password) {
}
