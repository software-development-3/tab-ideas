package com.tabideas.tabideas.dtos;

public record ResponseAuthenticationDTO(String token) {
}
