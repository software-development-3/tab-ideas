package com.tabideas.tabideas.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record RequestUserDTO(
        @NotNull @NotBlank
        String name,
        @NotNull @NotBlank @Email
        String login,
        @NotNull @NotBlank
        String password) {
}
