package com.tabideas.tabideas.dtos;

public record ResponseUserDTO(String id, String name, String login) {
}
