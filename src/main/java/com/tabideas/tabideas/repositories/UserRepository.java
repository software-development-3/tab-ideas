package com.tabideas.tabideas.repositories;

import com.tabideas.tabideas.entities.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends MongoRepository<UserEntity, String> {
    UserDetails findByLogin(String login);
}
