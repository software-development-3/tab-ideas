package com.tabideas.tabideas.controllers;

import com.tabideas.tabideas.dtos.AuthenticationDTO;
import com.tabideas.tabideas.dtos.RequestUserDTO;
import com.tabideas.tabideas.dtos.ResponseAuthenticationDTO;
import com.tabideas.tabideas.dtos.ResponseUserDTO;
import com.tabideas.tabideas.entities.UserEntity;
import com.tabideas.tabideas.factories.UserFactory;
import com.tabideas.tabideas.services.TokenService;
import com.tabideas.tabideas.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*", maxAge = 3600)
@Tag(name = "TabIdeas-user-api")
public class UserController {

    private final UserService userService;
    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;

    public UserController(UserService userService, TokenService tokenService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/register")
    @Operation(summary = "User registration", method = "POST")
    public ResponseEntity<ResponseUserDTO> register(@RequestBody @Valid RequestUserDTO dto){
        UserEntity newUser = new UserEntity();
        BeanUtils.copyProperties(dto, newUser);

        ResponseUserDTO response = UserFactory.convertUserToResponseDTO(userService.registerUser(newUser));

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PostMapping("/login")
    @Operation(summary = "Make Login", method = "POST")
    public ResponseEntity<ResponseAuthenticationDTO> login(@RequestBody @Valid AuthenticationDTO dto){
        var usernamePassword = new UsernamePasswordAuthenticationToken(dto.login(), dto.password());
        var auth = this.authenticationManager.authenticate(usernamePassword);

        var token = tokenService.generateToken((UserEntity) auth.getPrincipal());

        return ResponseEntity.ok(new ResponseAuthenticationDTO(token));
    }

    @GetMapping
    @Operation(summary = "Reading from all users", method = "GET")
    public ResponseEntity<Object> getAllUsers(){
        List<UserEntity> userList = userService.getAllUsers();

        if (userList.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No users in the database");
        }

        List<ResponseUserDTO> response = UserFactory.convertUsersToResponseDTO(userList);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "User search by ID", method = "GET")
    public ResponseEntity<ResponseUserDTO> getUserById(@PathVariable(name = "id") String id){
        UserEntity user = userService.getUserByID(id);
        ResponseUserDTO response = UserFactory.convertUserToResponseDTO(user);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete user", method = "DELETE")
    public ResponseEntity<String> deleteUser(@PathVariable(name = "id") String id){
        userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User deleted successfully");
    }

    @PutMapping("/{id}")
    @Operation(summary = "User update", method = "PUT")
    public ResponseEntity<ResponseUserDTO> updateUser(@PathVariable(name = "id") String id, @RequestBody @Valid RequestUserDTO dto){
        UserEntity userToUpdate = new UserEntity();
        BeanUtils.copyProperties(dto, userToUpdate);

        ResponseUserDTO response = UserFactory.convertUserToResponseDTO(userService.updateUser(id, userToUpdate));

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/verify/{token}")
    public ResponseEntity<String> verifyToken(@PathVariable String token){

        var subject =  tokenService.validateToken(token);
        if (subject.isEmpty()) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");

        return ResponseEntity.ok(subject);
    }
}
