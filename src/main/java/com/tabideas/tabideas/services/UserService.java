package com.tabideas.tabideas.services;

import com.tabideas.tabideas.Exceptions.UserRegisteredException;
import com.tabideas.tabideas.entities.UserEntity;
import com.tabideas.tabideas.repositories.UserRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserEntity registerUser(UserEntity user){
        if (userRepository.findByLogin(user.getLogin()) != null) throw new UserRegisteredException();
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userRepository.save(user);
    }

    public void deleteUser(String id){
        userRepository.findById(id).orElseThrow();
        userRepository.deleteById(id);
    }

    public List<UserEntity> getAllUsers(){
        return userRepository.findAll();
    }

    public UserEntity getUserByID(String id){
        return userRepository.findById(id).orElseThrow();
    }

    public UserEntity updateUser(String id, UserEntity user){
        var userToUpdate = userRepository.findById(id).orElseThrow();

        if(user.getName() != null) userToUpdate.setName(user.getName());
        if(user.getLogin() != null) userToUpdate.setName(user.getLogin());
        if(user.getPassword() != null) userToUpdate.setName(user.getPassword());

        return userToUpdate;
    }
}
