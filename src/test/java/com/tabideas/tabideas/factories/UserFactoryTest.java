package com.tabideas.tabideas.factories;

import com.tabideas.tabideas.dtos.ResponseUserDTO;
import com.tabideas.tabideas.entities.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserFactoryTest {

    UserEntity userEntity;

    List<UserEntity> userEntityList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        userEntity = new UserEntity("user1", "username1", "1234");

        for(int i = 2; i < 8; i++) {
            userEntityList.add(new UserEntity("user" + i, "username" + i, "1234"));
        }
    }

    @Test
    @DisplayName("It should be able to convert a list of UserEntity into ResponseDTO")
    void convertUsersToResponseDTO() {
        List<ResponseUserDTO> responseUserDTOs = UserFactory.convertUsersToResponseDTO(userEntityList);

        assertEquals(userEntityList.size(), responseUserDTOs.size());

        for (int i = 0; i < userEntityList.size(); i++) {
            assertEquals(userEntityList.get(i).getId(), responseUserDTOs.get(i).id());
            assertEquals(userEntityList.get(i).getName(), responseUserDTOs.get(i).name());
            assertEquals(userEntityList.get(i).getLogin(), responseUserDTOs.get(i).login());
        }
    }

    @Test
    @DisplayName("It should be able to convert a UserEntity into ResponseDTO")
    void convertUserToResponseDTO() {
        var responseDTO = UserFactory.convertUserToResponseDTO(userEntity);

        assertEquals(userEntity.getId(), responseDTO.id());
        assertEquals(userEntity.getName(), responseDTO.name());
        assertEquals(userEntity.getLogin(), responseDTO.login());
    }
}