package com.tabideas.tabideas.controllers;

import com.tabideas.tabideas.dtos.RequestUserDTO;
import com.tabideas.tabideas.dtos.ResponseUserDTO;
import com.tabideas.tabideas.entities.UserEntity;
import com.tabideas.tabideas.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    @Captor
    ArgumentCaptor<UserEntity> userArgumentCaptor;

    @BeforeEach
    void setUp() {
    }

    @Test
    @DisplayName("It should be able to register a new user")
    void registerUser() {
        RequestUserDTO requestDTO = new RequestUserDTO("John", "john@example.com", "password");
        UserEntity newUser = new UserEntity("John", "john@example.com", "password");
        ResponseUserDTO responseDTO = new ResponseUserDTO(newUser.getId(), "John", "john@example.com");

        when(userService.registerUser(userArgumentCaptor.capture())).thenReturn(newUser);

        ResponseEntity<ResponseUserDTO> responseEntity = userController.register(requestDTO);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(responseDTO, responseEntity.getBody());
        verify(userService, times(1)).registerUser(any());
    }

    @Test
    void getAllUsers() {
        List<UserEntity> userList = new ArrayList<>();
        userList.add(new UserEntity("John", "john@example.com", "password"));

        when(userService.getAllUsers()).thenReturn(userList);

        ResponseEntity<Object> responseEntity = userController.getAllUsers();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertInstanceOf(List.class, responseEntity.getBody());
        verify(userService, times(1)).getAllUsers();
    }

    @Test
    void getUserById() {
        UserEntity user = new UserEntity("John", "john@example.com", "password");
        ResponseUserDTO responseDTO = new ResponseUserDTO(user.getId(), "John", "john@example.com");

        when(userService.getUserByID(user.getId())).thenReturn(user);

        ResponseEntity<ResponseUserDTO> responseEntity = userController.getUserById(user.getId());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseDTO, responseEntity.getBody());
        verify(userService, times(1)).getUserByID(user.getId());
    }

    @Test
    void deleteUser() {
        String userId = UUID.randomUUID().toString();

        ResponseEntity<String> responseEntity = userController.deleteUser(userId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("User deleted successfully", responseEntity.getBody());
        verify(userService, times(1)).deleteUser(userId);
    }

    @Test
    void updateUser() {
        RequestUserDTO requestDTO = new RequestUserDTO("John Doe", "johndoe@example.com", "newpassword");
        UserEntity updatedUser = new UserEntity("John Doe", "johndoe@example.com", "newpassword");
        ResponseUserDTO responseDTO = new ResponseUserDTO(updatedUser.getId(), "John Doe", "johndoe@example.com");

        when(userService.updateUser(eq(updatedUser.getId()), any())).thenReturn(updatedUser);

        ResponseEntity<ResponseUserDTO> responseEntity = userController.updateUser(updatedUser.getId(), requestDTO);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseDTO, responseEntity.getBody());
        verify(userService, times(1)).updateUser(eq(updatedUser.getId()), any());
    }
}