package com.tabideas.tabideas.services;

import com.tabideas.tabideas.entities.UserEntity;
import com.tabideas.tabideas.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    @Captor
    ArgumentCaptor<UserEntity> userArgumentCaptor;

    UserEntity user;

    @BeforeEach
    void setUp() {
        user = new UserEntity("User", "username", "?+F£H252Nm");
    }

    @Test
    @DisplayName("It should be able to register a new user")
    void registerUser() {
        // Arrange
        doReturn(user).when(userRepository).save(userArgumentCaptor.capture());

        // Act
        when(userRepository.save(user)).thenReturn(user);

        UserEntity savedUser = userService.registerUser(user);

        // Assert
        assertEquals(user, savedUser);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void registerUserError() {
        // Arrange
        doThrow(new RuntimeException()).when(userRepository).save(any());
        var input = user;

        // Act & Assert
        assertThrows(RuntimeException.class, () -> {
            userService.registerUser(input);
        });
    }

    @Test
    @DisplayName("It should be able to delete a user")
    void deleteUser() {
        // Arrange
        var userId = UUID.randomUUID().toString();
        when(userRepository.findById(userId)).thenReturn(Optional.of(new UserEntity()));

        // Act & Assert
        assertDoesNotThrow(() -> userService.deleteUser(userId));
        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    @DisplayName("It should be able to retrieve all users on the system")
    void getAllUsers() {
        // Arrange
        List<UserEntity> userList = new ArrayList<>();
        userList.add(new UserEntity("John", "john@example.com", "password"));
        userList.add(new UserEntity("Alice", "alice@example.com", "password"));

        // Act
        when(userRepository.findAll()).thenReturn(userList);

        List<UserEntity> returnedList = userService.getAllUsers();

        // Assert
        assertEquals(userList.size(), returnedList.size());
        assertTrue(returnedList.containsAll(userList));
        verify(userRepository, times(1)).findAll();
    }

    @Test
    @DisplayName("It should be able to retrieve user data by id")
    void getUserByID() {
        // Arrange
        String userId = UUID.randomUUID().toString();
        UserEntity user = new UserEntity("John", "john@example.com", "password");

        // Act
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        UserEntity returnedUser = userService.getUserByID(userId);

        // Assert
        assertEquals(user, returnedUser);
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    @DisplayName("It should be able to update data from a user")
    void updateUser() {
        // Arrange
        String userId = "1";
        UserEntity existingUser = new UserEntity("John", "john@example.com", "password");
        UserEntity updatedUser = new UserEntity("John Doe", null, null);

        // Act
        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));

        UserEntity returnedUser = userService.updateUser(userId, updatedUser);

        // Assert
        assertEquals(updatedUser.getName(), returnedUser.getName());
        assertEquals(existingUser.getLogin(), returnedUser.getLogin());
        assertEquals(existingUser.getPassword(), returnedUser.getPassword());
        verify(userRepository, times(1)).findById(userId);
    }
}